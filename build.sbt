name := "Casumo"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.4.0"
val scalaTestVersion = "3.0.1"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % sparkVersion % "compile",
  "org.apache.spark" %% "spark-avro" % sparkVersion % "compile",
  "org.apache.spark" %% "spark-hive" % sparkVersion % "compile",
  "org.apache.spark" %% "spark-streaming" % sparkVersion % "compile",
  "org.scalatest" %% "scalatest" % scalaTestVersion % "test"
)
